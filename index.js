kontra.init()
c = kontra.canvas
level1 = {
	bonkieStart: {
		x: 200,
		y: 100
	}
}

let wall = kontra.sprite({
	x: 400,
	y: 50,
	width: 1,
	height: 200,
	color: 'white',
	render() {
		this.draw()
	}
})
let bonkie = kontra.sprite({
	x: level1.bonkieStart.x,
	y: level1.bonkieStart.y,
	width: 92,
	height: 92,
	radians: 0, // 2π rad (~6,28) is a full circle, calling it radians to 
	// prevent collisiondetection from failing
	color: 'red',
	anchor: {
		x: .5,
		y: .5
	},
	thrusters() {
		let magnitude = Math.sqrt(this.dx*this.dx+this.dy*this.dy)
		if(magnitude > 2) {
			this.dy *= .85
			this.dx *= .85
		}

		if(this.x < 0) this.x = 0
		else if(this.x > c.width) this.x = c.width
		if(this.y > c.height) this.y = c.height
		else if(this.y < 0) this.y = 0
	},
	fireThrusters() {
		this.ddx = Math.cos(this.radians) * 0.1
		this.ddy = Math.sin(this.radians) * 0.1
	},
	dampenThrusters() {
		this.ddx = 0
		this.ddy = 0
	},
	stabilizers() {
		this.dampenThrusters()
		this.dx = this.dx / 1.025
		this.dy = this.dy / 1.025
	},
	detectWalls() {
		if(this.collidesWith(wall)) {

			this.x = this.x - this.dx // Return to 1px before so movement is possible
			this.y = this.y - this.dy // Return to 1px before so movement is possible
			this.ddx = 0
			this.ddy = 0
			this.dy = 0
			this.dx = 0
		}
	},
	render() {
		let ctx = this.context
		// this.draw() // Debug
		ctx.strokeStyle = "white"

		ctx.save()
		ctx.translate(this.x, this.y)
		ctx.rotate(this.radians)
		ctx.translate(-this.x, -this.y)

		ctx.beginPath()
		ctx.arc(this.x, this.y, this.width / 2, 0, Math.PI*2)
		
		ctx.moveTo(this.x - 10, this.y + 6)
		ctx.lineTo(this.x - 10, this.y - 6)
		ctx.lineTo(this.x + 10, this.y)
		ctx.closePath()
		ctx.stroke()
		ctx.restore()
		ctx.closePath()
		
	}
})
let cleanedPath = {
	// eg x40y40: true
}
let cleaned = kontra.sprite({
	x: 0,
	y: 0,
	show: true,
	render() {
		if(this.show) {
			let ctx = kontra.context
			ctx.beginPath()
			ctx.moveTo(level1.bonkieStart.x, level1.bonkieStart.y)
			Object.keys(cleanedPath).forEach((k)=>{
				ctx.lineTo(cleanedPath[k].x, cleanedPath[k].y)
			})
			ctx.strokeStyle = "lightblue"
			ctx.lineWidth = "20"
			ctx.lieCap = "round"
			ctx.stroke()
			ctx.closePath()
			ctx.lineWidth = "1"
		}
	}
})

// Deal with keypresses
function updateKeyPresses() {
	if(kontra.keys.pressed('a')) 
		bonkie.radians += .1
	if(kontra.keys.pressed('d')) 
		bonkie.radians -= .1
	if(kontra.keys.pressed('w')) 
		bonkie.fireThrusters()
	if(kontra.keys.pressed('s')) 
		bonkie.stabilizers()

	let gp = navigator.getGamepads()
	if(gp.length && gp[0].id.length > 1) {
		if(gp[0].axes[0] === 1) // right / left
			bonkie.radians += .1
		if(gp[0].axes[0] === -1) // right / left
			bonkie.radians -= .1
		if(gp[0].buttons[1]) // up / down
			bonkie.fireThrusters()
		if(gp[0].buttons[2]) // up / down
			bonkie.stabilizers()
		else
			bonkie.dampenThrusters()
	}

	// Debug keys
	if(kontra.keys.pressed('q')) {
		// cleaned.show = true
		// console.log(Math.round(new Date() % 1000 / 16.666)) // Frames
		// console.log(Object.keys(cleanedPath).length)
		// console.log((cleanedPath))
	}
}
kontra.keys.bind('space', ()=>{
	if(loop.isStopped) loop.start()
	else(loop.stop())
})
window.addEventListener('gamepadconnected', (e) => {
	console.log(e)
	window.gamepads.innerText = "[1]"
})

// Gameloop
let loop = kontra.gameLoop({
	update() {
		wall.update()
		bonkie.detectWalls()
		bonkie.thrusters()
		bonkie.update()

		let y = Math.floor(bonkie.y)
		let x = Math.floor(bonkie.x)

		// cleanedPath[`x${x}y${y}`] = true
		cleanedPath[`x${x}y${y}`] = {
			x,y
		}

		updateKeyPresses()
	},
	render() {
		wall.render()
		bonkie.render()
		cleaned.render()
	}
})
loop.start()
