// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

// eslint-disable-next-line no-global-assign
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  return newRequire;
})({"index.js":[function(require,module,exports) {
kontra.init();
c = kontra.canvas;
level1 = {
  bonkieStart: {
    x: 200,
    y: 100
  }
};
var wall = kontra.sprite({
  x: 400,
  y: 50,
  width: 1,
  height: 200,
  color: 'white',
  render: function render() {
    this.draw();
  }
});
var bonkie = kontra.sprite({
  x: level1.bonkieStart.x,
  y: level1.bonkieStart.y,
  width: 92,
  height: 92,
  radians: 0,
  // 2π rad (~6,28) is a full circle, calling it radians to 
  // prevent collisiondetection from failing
  color: 'red',
  anchor: {
    x: .5,
    y: .5
  },
  thrusters: function thrusters() {
    var magnitude = Math.sqrt(this.dx * this.dx + this.dy * this.dy);

    if (magnitude > 2) {
      this.dy *= .85;
      this.dx *= .85;
    }

    if (this.x < 0) this.x = 0;else if (this.x > c.width) this.x = c.width;
    if (this.y > c.height) this.y = c.height;else if (this.y < 0) this.y = 0;
  },
  fireThrusters: function fireThrusters() {
    this.ddx = Math.cos(this.radians) * 0.1;
    this.ddy = Math.sin(this.radians) * 0.1;
  },
  dampenThrusters: function dampenThrusters() {
    this.ddx = 0;
    this.ddy = 0;
  },
  stabilizers: function stabilizers() {
    this.dampenThrusters();
    this.dx = this.dx / 1.025;
    this.dy = this.dy / 1.025;
  },
  detectWalls: function detectWalls() {
    if (this.collidesWith(wall)) {
      this.x = this.x - this.dx; // Return to 1px before so movement is possible

      this.y = this.y - this.dy; // Return to 1px before so movement is possible

      this.ddx = 0;
      this.ddy = 0;
      this.dy = 0;
      this.dx = 0;
    }
  },
  render: function render() {
    var ctx = this.context; // this.draw() // Debug

    ctx.strokeStyle = "white";
    ctx.save();
    ctx.translate(this.x, this.y);
    ctx.rotate(this.radians);
    ctx.translate(-this.x, -this.y);
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.width / 2, 0, Math.PI * 2);
    ctx.moveTo(this.x - 10, this.y + 6);
    ctx.lineTo(this.x - 10, this.y - 6);
    ctx.lineTo(this.x + 10, this.y);
    ctx.closePath();
    ctx.stroke();
    ctx.restore();
    ctx.closePath();
  }
});
var cleanedPath = {// eg x40y40: true
};
var cleaned = kontra.sprite({
  x: 0,
  y: 0,
  show: true,
  render: function render() {
    if (this.show) {
      var ctx = kontra.context;
      ctx.beginPath();
      ctx.moveTo(level1.bonkieStart.x, level1.bonkieStart.y);
      Object.keys(cleanedPath).forEach(function (k) {
        ctx.lineTo(cleanedPath[k].x, cleanedPath[k].y);
      });
      ctx.strokeStyle = "lightblue";
      ctx.lineWidth = "20";
      ctx.lieCap = "round";
      ctx.stroke();
      ctx.closePath();
      ctx.lineWidth = "1";
    }
  }
}); // Deal with keypresses

function updateKeyPresses() {
  if (kontra.keys.pressed('a')) bonkie.radians += .1;
  if (kontra.keys.pressed('d')) bonkie.radians -= .1;
  if (kontra.keys.pressed('w')) bonkie.fireThrusters();
  if (kontra.keys.pressed('s')) bonkie.stabilizers();
  var gp = navigator.getGamepads();

  if (gp.length && gp[0].id.length > 1) {
    if (gp[0].axes[0] === 1) // right / left
      bonkie.radians += .1;
    if (gp[0].axes[0] === -1) // right / left
      bonkie.radians -= .1;
    if (gp[0].buttons[1]) // up / down
      bonkie.fireThrusters();
    if (gp[0].buttons[2]) // up / down
      bonkie.stabilizers();else bonkie.dampenThrusters();
  } // Debug keys


  if (kontra.keys.pressed('q')) {// cleaned.show = true
    // console.log(Math.round(new Date() % 1000 / 16.666)) // Frames
    // console.log(Object.keys(cleanedPath).length)
    // console.log((cleanedPath))
  }
}

kontra.keys.bind('space', function () {
  if (loop.isStopped) loop.start();else loop.stop();
});
window.addEventListener('gamepadconnected', function (e) {
  console.log(e);
  window.gamepads.innerText = "[1]";
}); // Gameloop

var loop = kontra.gameLoop({
  update: function update() {
    wall.update();
    bonkie.detectWalls();
    bonkie.thrusters();
    bonkie.update();
    var y = Math.floor(bonkie.y);
    var x = Math.floor(bonkie.x); // cleanedPath[`x${x}y${y}`] = true

    cleanedPath["x".concat(x, "y").concat(y)] = {
      x: x,
      y: y
    };
    updateKeyPresses();
  },
  render: function render() {
    wall.render();
    bonkie.render();
    cleaned.render();
  }
});
loop.start();
},{}],"../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "52434" + '/');

  ws.onmessage = function (event) {
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      console.clear();
      data.assets.forEach(function (asset) {
        hmrApply(global.parcelRequire, asset);
      });
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          hmrAccept(global.parcelRequire, asset.id);
        }
      });
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAccept(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAccept(bundle.parent, id);
  }

  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAccept(global.parcelRequire, id);
  });
}
},{}]},{},["../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","index.js"], null)
//# sourceMappingURL=/bonkie.e31bb0bc.map